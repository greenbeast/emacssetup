;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
;(require 'package)
;(package-initialize)
;(add-to-list 'package-archives 
   	    ;'("marmalade" . "http://marmalade-repo.org/packages/")
;	     '("melpa-stable" . "https://stable.melpa.org/"))
;(package-initialize)
;;The lines just below add melpa and marmalade packages available for download.
(require 'package)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives
	     '("marmalade" . "http://marmalade-repo.org/packages/"))
(add-to-list 'package-archives
	     '("gnu" . "https://elpa.gnu.org/packages/"))
(package-initialize)
					;(require 'package)
;(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
;                    (not (gnutls-available-p))))
;       (proto (if no-ssl "http" "https")))
;  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
;  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")))
;  ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) )
;  (when (< emacs-major-version 24)
;    ;; For important compatibility libraries like cl-lib
;    (add-to-list 'package-archives '("gnu" . (concat proto "://elpa.gnu.org/packages/")))))
;(package-initialize)

;;Install EIN which is basically jupyter notebook but for emacs.

;; Install Elpy, magit, blacken, ivy, org-alert,org-wild-notifier, use-package by M-x package-install 'use-package', wakatime which is a productivity tracker for emacs.

;; At this point I have no clue if it is actually anything worth it so I am testing it right now. You basically enable it then it records your productivity and gives a visual display on it.
;;To get auto complete working install elpy then do =M-x + elpy-config= and change Python path if it isn't working.
;;Then do =M-x + elpy-black-fix-code=
;;Install =treemacs=, this thing is dope.
;;============================================
;;Treemacs https://github.com/Alexander-Miller/treemacs
;;============================================
;;Multiple cursors
;; https://github.com/magnars/multiple-cursors.el
;;Install Ace-Jump-Mode. People keep raving about this so I am abou tto test it out.
;; use C-c SPC then enter a word or part of a word
;;Yep, it is pretty fucking nice. Install ace-popup-menu as well.
(global-set-key (kbd "C-c SPC") 'ace-jump-char-mode) 
(require 'ace-popup-menu)
(ace-popup-menu-mode 1)
;The above isn't working. I think it is because Smex is enabled.

(require 'multiple-cursors);M-x occur goes great with this.

(global-set-key (kbd "C-S-c C-s-c") 'mc/edit-lines)

(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
(global-unset-key (kbd "M-<down-mouse-1>"))
(global-set-key (kbd "M-<mouse-1>") 'mc/add-cursor-on-click);mouse-1 means left clicking with my mouse.

;;Change the cursor color, this is pretty cool.

(defvar blink-cursor-colors (list  "#92c48f" "#6785c5" "#be369c" "#d9ca65")
  "On each blink the cursor will cycle to the next color in this list.")

(setq blink-cursor-count 0)
(defun blink-cursor-timer-function ()
  "Zarza wrote this cyberpunk variant of timer `blink-cursor-timer'. 
Warning: overwrites original version in `frame.el'.

This one changes the cursor color on each blink. Define colors in `blink-cursor-colors'."
  (when (not (internal-show-cursor-p))
    (when (>= blink-cursor-count (length blink-cursor-colors))
      (setq blink-cursor-count 0))
    (set-cursor-color (nth blink-cursor-count blink-cursor-colors))
    (setq blink-cursor-count (+ 1 blink-cursor-count))
    )
  (internal-show-cursor nil (not (internal-show-cursor-p)))
  )

;;Flymake
(setq flymake-max-parallel-syntax-checks 8)

(setq flymake-number-of-erros-to-display nil)


;;Symon
(require 'symon)
(symon-mode)


;imenu-anywhere set-up
(global-set-key (kbd "C-.") #'imenu-anywhere)

;;imenu-list set-up, might be a better alternative to imenu-anywhere
;https://github.com/bmag/imenu-list
(global-set-key (kbd "C-'") #'imenu-list-smart-toggle)

;;Getting rid of line wrapping.
(set-default 'truncate-lines t)


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#d2ceda" "#f2241f" "#67b11d" "#b1951d" "#3a81c3" "#a31db1" "#21b8c7" "#655370"])
 '(custom-safe-themes
   '("bffa9739ce0752a37d9b1eee78fc00ba159748f50dc328af4be661484848e476" "fa2b58bb98b62c3b8cf3b6f02f058ef7827a8e497125de0254f56e373abee088" default))
 '(hl-todo-keyword-faces
   '(("TODO" . "#dc752f")
     ("NEXT" . "#dc752f")
     ("THEM" . "#2d9574")
     ("PROG" . "#3a81c3")
     ("OKAY" . "#3a81c3")
     ("DONT" . "#f2241f")
     ("FAIL" . "#f2241f")
     ("DONE" . "#42ae2c")
     ("NOTE" . "#b1951d")
     ("KLUDGE" . "#b1951d")
     ("HACK" . "#b1951d")
     ("TEMP" . "#b1951d")
     ("FIXME" . "#dc752f")
     ("XXX+" . "#dc752f")
     ("\\?\\?\\?+" . "#dc752f")))
 '(package-selected-packages
   '(highlight-symbol julia-mode elpy ivy-prescient spacemacs-theme spaceline centaur-tabs use-package nyan-mode swiper symon multiple-cursors smex ace-popup-menu))
 '(pdf-view-midnight-colors '("#655370" . "#fbf8ef")))



;;Elfeed to listen to podcasts within emacs
(global-set-key (kbd "C-x w") 'elfeed)
;
;(setq ravar/elfeed-podcast-dir "C:/Users/Hank/Music/Podcasts")
;(defun ravar/elfeed-play-enclosure-mpd ()
;  "Downloads the item in the enclosure and starts playing once in mpd using mpc"
;  (interactive)
;  (let* ((entry elfeed-show-entry)
;	 (enclosure-index (elfeed--get-enclosure-num
;			   "Enclosure to save" entry))
;	 (url-enclosure (car (elt (elfeed-entry-enclosures entry)
;				  (- enclosure index 1))))
;	 (fname
;	  (funcall elfeed-show-enclosure-filename-function
;		   entry url-enclosure)))
;    (start-process-shell-command
;     "Play enclosure" nil
;     (format "cd %s; curl %s;mpc update; mpc search filename %s | mpc insert; mpc next"
;	     ravar/elfeed-podcast-dir url-enclosure fname))))
;(define-key elfeed-show-mode-map (kbd "o") 'ravar/elfeed-play-enclosure-mpd)

;======================================================================
;https://github.com/emacs-tw/awesome-emacs
;======================================================================

;;SEt up EMMS to play mp3 files inside of emacs
;(use-package emms
;  :ensure t
;  :config)
;(require 'emms-setup)
;(require 'emms-player-mplayer)
;(emms-all)
;(setq emms-player-list '(
;                         emms-player-mpg321
;                         emms-player-ogg123
;                         emms-player-mplayer))
;(defun emms-player-mplayer-volume(amount)
;  (process-send-string
;   emms-player-simple-process-name
;   (format "volume %d\n" amount)))
;(setq emms-volume-change-function 'emms-player-mplayer-volume)
;(emms-add-directory-tree emms-source-file-default-directory)
;)



;;Bruh, hyperspace is dope. 
;;Changing key binding for minor mode
;; Check out the code to see the plethera of different ways to search stuff.
;;https://github.com/ieure/hyperspace-el/blob/master/hyperspace.el such as yt
(eval-after-load "hyperspace"
  (lambda () (define-key hyperspace-minor-mode-map "\C-ca" #'hyperspace)))
(global-set-key (kbd "C-c s") 'hyperspace);;C-c s is now hyper space enter and is a beatutiful thing.

;; Global key bind to set 'g' for git in magit
(global-set-key (kbd"C-x g") 'magit-status)


;Centaur Tabs https://github.com/ema2159/centaur-tabs
(use-package centaur-tabs
  :demand
  :config
  (centaur-tabs-mode t)
  :bind
  ("C-<prior>" . centaur-tabs-backward)
  ("C-<next>" . centaur-tabs-forward))
(centaur-tabs-headline-match)
(setq centaur-tabs-style "slant")
(setq centaur-tabs-set-icons t)
(setq centaur-tabs-set-bar 'under)
(setq centaur-tabs-close-button "X")
(setq centaur-tabs-set-modified-marker t)
(setq centaur-tabs-modified-marker "*")
(global-set-key (kbd "<C-tab>") 'centaur-tabs-counsel-switch-group);Opens the list of groups that my tabs are in. If I can remember to use it then it would be super helpful.


;; Global key bind to my init file. This will obviously need to have the path changed for the specific location of the init file.
(global-set-key (kbd"C-x i") (lambda () (interactive)(find-file "/home/hank/.emacs")))

;;Global key bind to my postgres development space
(global-set-key (kbd"C-x p") (lambda () (interactive)(find-file "/home/hank/Development/postgres-odbc/tkinterdatePsycopg.py")))

;; Global key bind to my Dev Thought Org Mode 
(global-set-key (kbd"C-x d") (lambda () (interactive)(find-file "C:\\Users\\Hank\\Emacs(DontTouch)\\DevThoughts.org")))

;; Mute that annoying ass beep
(setq visible-bell 1)

;;Adding WindMove to make it so that hold shift and hitting the arrows moves through
;;the available buffers rather than hitting C-x o
(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings))

;;Spaceline
(require 'spaceline-config)
(spaceline-spacemacs-theme)
;(use-package spaceline-config :ensure spaceline
;  :config
;  (spaceline-ivy-mode 1)
;  (spaceline-install
;   'main
;   '((buffer-modified)
;     ((remote-host buffer-id) :face highlight-face)
;     (process :when active))
;   '((selection-info :face 'region :when mark-active)
;     ((flycheck-error flycheck-warning flycheck-info) :when active)
;     (which-function)
;     (version-control :when active)
;     (line-column)
;     (global :when active)
;     (major-mode))))

(setq-default
 powerline-height 24
 powerline-default-separator 'wave
 spaceline-separator-dir-left '(right . right)
 spaceline-separator-dir-right '(left . left))

;; -*- mode: elisp -*-
;; Disable the splash screen(to enable again replace t with 0)
(setq inhibit-splash-screen t)

;;============================
;; Org mode configuration
;;============================
;; Enable org mode
(require 'org)
;;Should let YaSnippet work within org-mode, fingers crossed.
;;This works but to enable snippets you have to do =M-x + yas-minor-mode-on= and snippets will work.
(defun my-org-mode-hook ()
  (setq-local yas-buffer-local-condition
              '(not (org-in-src-block-p t))))
(add-hook 'org-mode-hook #'my-org-mode-hook)

;;Auto-YASnippet Basically sets up a snippet in the same way a macro does but automatically stores it as a snippet
(global-set-key (kbd "C-a") #'aya-create)
(global-set-key (kbd "C-e") #'aya-expand)
(global-set-key (kbd "C-o") #'aya-open-line)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Bruh, I need to do a bit more research on this. 
;Setting the windows key to a hyper key while inside of emacs. currently isn't working for Windows 10.
(setq w32-pass-apps-to-system nil)
(setq w32-apps-modifier 'hyper) ; Menu/App key

;; fix some org-mode + yasnippet conflicts:
(defun yas/org-very-safe-expand ()
  (let ((yas/fallback-behavior 'return-nil)) (yas/expand)))

(add-hook 'org-mode-hook
          (lambda ()
            (make-variable-buffer-local 'yas/trigger-key)
            (setq yas/trigger-key [tab])
            (add-to-list 'org-tab-first-hook 'yas/org-very-safe-expand)
            (define-key yas/keymap [tab] 'yas/next-field)))


;; customizations from https://zzamboni.org/post/beautifying-org-mode-in-emacs/ 
;;Hides the markup emphasis ie ** or _ _ or //
(setq org-hide-emphasis-markers t)

(setq org-todo-keywords
      '((sequence "TODO" "IN-PROGRESS" "WAITING" "DONE")))

(global-set-key "\C-ca" 'org-agenda)

(setq org-tag-alist
      '(("@work" . ?w)("@LowPriority" . ?L) ("@MediumPriority" . ?M)("@HighPriority" . ?H) ("@home" . ?h) ("laptop" . ?l)))



;;Smex which is basically a better version of M-x
(require 'smex) ; Not needed if you use package.el
(smex-initialize) ; Can be omitted. This might cause a (minimal) delay
                    ; when Smex is auto-initialized on its first run.

(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)
;; This is the old M-x.
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)

;;Nyan-mode
(require 'nyan-mode)
(nyan-mode 1)
;(nyan-start-animation 1)
;This makes it so that the eshell is open when emacs opens.
(eshell 1)

;;======================
;; Development Setup
;;======================
(require 'use-package)

;;This is for Ivy
(ivy-prescient-mode 1)
(use-package ivy
  :demand
  :config
  (setq ivy-use-virtual-buffers t
	ivy-count-format "d/%d"))
(ivy-mode 1) ;;This sets ivy-mode on by default
(global-set-key (kbd "C-s") 'swiper) ;;Replaces I-search with Swiper because I-search is garbage honestly.
;; To scroll up or down in swiper do C-p and C-n and to open swiper use C-s
(global-linum-mode t)
(elpy-enable)
;;Hopefully gets blacken to work
(use-package blacken
    :hook (python-mode . blacken-mode)
    :config
    (setq blacken-line-length '88))

;;Should fix the indentation issue with yasnippet, it works, thank god. That was so damn annoying
(setq yas-indent-line nil);;Doesn't solve the issue inside of a function. FML.

(bind-key "C-c C-a" 'elpy-black-fix-code)
;;(use-package direnv
;; :config
;; (direnv-mode))

;;Gives the ability to run Julia code
(require 'julia-mode)

;;Symbol hightlighting [https://github.com/nschum/highlight-symbol.el]
;;Enable the highlight-symbol-mode to have it highlight every occurance of the word your cursor is on.
(require 'highlight-symbol)
(global-set-key [(control f3)] 'highlight-symbol);;This is super useful
(global-set-key [f3] 'highlight-symbol-next)
(global-set-key [(shift f3)] 'highlight-symbol-prev)
(global-set-key [(meta f3)] 'hightlight-symbol-query-replace)

;;SEt Ranger shortcut
(global-set-key (kbd "C-c r") 'ranger)

;;=================================
;;Org mode customizations
;;=================================
(setq org-export-with-section-numbers nil);;Gets rid of the numbered sections
(setq org-list-description-max-indents 5) ;; Sets max indentation for description lists to 5

;;Enable python code evaluation in org-mode
(org-babel-do-load-languages
 'org-babel-load-languages '((python . t)))
(require 'python)

(setq org-adapt-indentation nil) ;;Prevent demoting heading also shifting text inside sections.
;;Auto compelete
;; Completion words longer than 4 characters

(eval-after-load "auto-complete"
 '(progn
     (ac-config-default)))

(add-hook 'git-commit-mode-hook 'ac-ispell-ac-setup)
(add-hook 'mail-mode-hook 'ac-ispell-ac-setup)

;;=======================================
;;

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
